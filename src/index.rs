use std::collections::btree_map::BTreeMap as Map;
use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};

use semver::{Version, VersionReq};
use serde_json::{self, Value};

fn get_crate_index_path(name: &str) -> PathBuf {
    match name.len() {
        0 => panic!("invalid empty crate name"),
        1 => Path::new("1").join(name),
        2 => Path::new("2").join(name),
        3 => Path::new("3").join(&name[0..1]).join(name),
        _ => Path::new(&name[..2]).join(&name[2..4]).join(name),
    }
}

pub struct IndexCache {
    crate_index_map: Map<String, IndexCrate>,
}

impl IndexCache {
    pub fn new(crate_index_map: Map<String, IndexCrate>) -> IndexCache {
        IndexCache { crate_index_map }
    }

    pub fn get_required<'a>(&'a self, name: &str, req: &VersionReq) -> Option<&'a IndexEntry> {
        self.get_index_crate(&name)?.get_match(req)
    }

    pub fn get_index_crate<'a>(&'a self, name: &str) -> Option<&'a IndexCrate> {
        self.crate_index_map.get(&name.to_lowercase())
    }

    pub fn count(&self) -> usize {
        self.crate_index_map.len()
    }

    pub fn iter_indexed_crates(&self) -> impl Iterator<Item = &IndexCrate> {
        self.crate_index_map.values()
    }
}

/// All indexed versions of a specific crate.
#[derive(Default)]
pub struct IndexCrate {
    /// List of crate versions, in ascending version order (oldest version first).
    entries: Vec<IndexEntry>,
}

impl IndexCrate {
    pub fn from_file(crate_index_path: &Path) -> IndexCrate {
        let crate_index_file =
            BufReader::new(File::open(crate_index_path).expect("failed to open index file"));
        IndexCrate::new(
            crate_index_file
                .lines()
                .map(|l| {
                    serde_json::from_str(&l.expect("failed to read index file"))
                        .expect("failed to parse index file")
                }).collect(),
        )
    }

    pub fn new(entries: Vec<IndexEntry>) -> IndexCrate {
        IndexCrate { entries }
    }

    pub fn newest_entry(&self) -> Option<&IndexEntry> {
        self.entries.last()
    }

    pub fn get_match<'a>(&'a self, req: &VersionReq) -> Option<&'a IndexEntry> {
        self.entries
            .iter()
            .rev()
            .find(|entry| !entry.is_yanked() && req.matches(&entry.vers))
            .or_else(|| {
                // Check again for versions that may have been yanked.
                self.entries
                    .iter()
                    .rev()
                    .find(|entry| req.matches(&entry.vers))
            })
    }
}

/// A single indexed version of a specific crate.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct IndexEntry {
    pub name: String,
    pub vers: Version,
    pub deps: Vec<IndexEntryDep>,
    pub features: Map<String, Vec<String>>,
    pub cksum: String,
    pub yanked: Option<bool>,
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

impl IndexEntry {
    pub fn is_yanked(&self) -> bool {
        self.yanked == Some(true)
    }
}

impl fmt::Display for IndexEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.name, self.vers)
    }
}

/// A dependency of an indexed crate version.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct IndexEntryDep {
    pub name: String,
    pub req: String,
    pub optional: bool,
    pub features: Vec<String>,
    pub default_features: bool,
    pub kind: Option<String>,
    pub target: Option<String>,
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

impl fmt::Display for IndexEntryDep {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let optional_mark = if self.optional { "?" } else { "" };
        write!(f, "{}{} {}", self.name, optional_mark, self.req)
    }
}
