pub const UPSTREAM_CRATES_URL: &str = "https://github.com/rust-lang/crates.io-index.git";
pub const CRATES_AUDIT_INDEX: &str = "crates-audit-index";
